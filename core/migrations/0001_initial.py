# Generated by Django 2.0 on 2019-09-17 10:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Quarto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descricao', models.CharField(max_length=255)),
                ('observacao', models.CharField(blank=True, max_length=255, null=True)),
                ('valor', models.FloatField(null=True)),
                ('data_inicio', models.CharField(max_length=255, null=True)),
                ('data_fim', models.CharField(max_length=255, null=True)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Reserva',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inicio', models.CharField(max_length=255, null=True)),
                ('fim', models.CharField(max_length=255, null=True)),
                ('cliente', models.CharField(max_length=255)),
                ('observacao', models.CharField(blank=True, max_length=255, null=True)),
                ('ativo', models.BooleanField(default=True)),
                ('descricao_quarto', models.CharField(max_length=255)),
                ('valor_quarto', models.FloatField()),
                ('quarto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Quarto', to='core.Quarto')),
            ],
        ),
    ]
